<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('watch_id')->nullable();
            $table->foreign('watch_id')
                ->references('id')
                ->on('watches')
                ->onDelete('cascade');
            $table->string('property_type')->index();
            $table->smallInteger('size')->unsigned();
            $table->tinyInteger('rooms')->unsigned();
            $table->string('location');
            $table->string('img');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
