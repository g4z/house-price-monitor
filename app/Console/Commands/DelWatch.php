<?php

namespace App\Console\Commands;

use App\Watch;
use Illuminate\Console\Command;

class DelWatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hpm:del {reference}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a currently watched reference';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reference = $this->argument('reference');

        $watch = Watch::where('reference', $reference)->first();

        if (!$watch) {
            $this->error('Reference doesn\'t exist!');
            return 1;
        }

        Watch::where([
            'reference' => $reference
        ])->delete();

        $this->info('Reference deleted!');

        return 0;
    }
}
