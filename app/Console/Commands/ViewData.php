<?php

namespace App\Console\Commands;

use App\House;
use App\Price;
use App\Watch;
use Exception;
use Illuminate\Console\Command;

class ViewData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hpm:view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show data in a browser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $f = tempnam(sys_get_temp_dir(), uniqid());

        file_put_contents($f, '<html><body><table>');

        foreach (Watch::all() as $watch) {

            file_put_contents($f, '<tr>', FILE_APPEND);
            file_put_contents($f, sprintf('<td>URL:</td>'), FILE_APPEND);
            file_put_contents($f, sprintf('<td><a href="https://www.immobiliare.it/annunci/%s/" target="_blank">Open webpage</a></td>', $watch->reference), FILE_APPEND);
            file_put_contents($f, '</tr><tr>', FILE_APPEND);

            if ($watch->house) {

                file_put_contents($f, sprintf('<td>Spec:</td>'), FILE_APPEND);
                file_put_contents($f, sprintf('<td>%s - %s mq - %s Rooms</td>', $watch->house->property_type, $watch->house->size, $watch->house->rooms), FILE_APPEND);
                file_put_contents($f, '</tr><tr>', FILE_APPEND);
                file_put_contents($f, sprintf('<td>Location:</td>'), FILE_APPEND);
                file_put_contents($f, sprintf('<td>%s</td>', $watch->house->location), FILE_APPEND);
                file_put_contents($f, '</tr><tr>', FILE_APPEND);
                // file_put_contents($f, sprintf('<td>Description:</td>'), FILE_APPEND);
                // file_put_contents($f, sprintf('<td>%s</td>', $watch->house->description), FILE_APPEND);
                // file_put_contents($f, '</tr><tr>', FILE_APPEND);
                file_put_contents($f, sprintf('<td>&nbsp;</td>'), FILE_APPEND);
                file_put_contents($f, sprintf('<td><img src="%s"></td>', $watch->house->img), FILE_APPEND);
                file_put_contents($f, '</tr><tr>', FILE_APPEND);

                file_put_contents($f, sprintf('<td>&nbsp;</td>'), FILE_APPEND);
                file_put_contents($f, sprintf('<td><ul>'), FILE_APPEND);

                $prices = $watch->house->prices()->orderBy('id', 'desc')->get();
                foreach ($prices as $price) {
                    file_put_contents($f, sprintf('<li>%s: €%s</li>', $price->created_at, number_format($price->amount, 0)), FILE_APPEND);
                }
                file_put_contents($f, sprintf('</td>'), FILE_APPEND);
                file_put_contents($f, '</tr><tr>', FILE_APPEND);

            } else {

                file_put_contents($f, sprintf('<td>ERROR</td>'), FILE_APPEND);
                file_put_contents($f, sprintf('<td>NOT FOUND IN SEARCH</td>'), FILE_APPEND);
                file_put_contents($f, '</tr><tr>', FILE_APPEND);
            }


            file_put_contents($f, '<td><br><br></td>', FILE_APPEND);
            file_put_contents($f, '</tr>', FILE_APPEND);
        }

        file_put_contents($f, '</table></body></html>', FILE_APPEND);

        exec(sprintf('sensible-browser --new-tab %s &', $f));

        return 0;
    }
}
