<?php

namespace App\Console\Commands;

use App\House;
use App\Price;
use App\Watch;
use Exception;
use Illuminate\Console\Command;

class RefreshData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hpm:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh data from immobiliare.it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $urlFormatStr = 'https://www.immobiliare.it/ricerca.php?idCategoria=1&idContratto=1&idTipologia%5B0%5D=7&idTipologia%5B1%5D=12&idNazione=IT&prezzoMinimo=120000&prezzoMassimo=200000&criterio=dataModifica&ordine=desc&pag={{page}}&vrt=43.33167102675841,12.112426757812502;43.28020470342188,12.030715942382814;43.159613,11.966171;43.07089421067248,11.984710693359375;42.97953485978903,12.076721191406252;42.9524020856897,12.227783203125002;42.9735063886709,12.39532470703125;43.068887774169625,12.470855712890627;43.07591001456969,12.350006103515627;43.10549490130811,12.357559204101564;43.1355665702956,12.392578125;43.1450861841603,12.490081787109377;43.24620305791017,12.529907226562502;43.30019680023961,12.426910400390627;43.29619890659104,12.32597351074219;43.32967314784175,12.265548706054688&mode=rss';
        $urlFormatStr = 'https://www.immobiliare.it/ricerca.php?idCategoria=1&idContratto=1&idTipologia%5B0%5D=7&idTipologia%5B1%5D=12&idNazione=IT&prezzoMinimo=100000&prezzoMassimo=200000&criterio=dataModifica&ordine=desc&pag={{page}}&vrt=43.09223361491757,12.346572875976564;43.09423928702563,12.325286865234377;43.10100794544337,12.295074462890627;43.10226131859784,12.268638610839846;43.103263998646966,12.208900451660158;43.12807509686775,12.20787048339844;43.14886856810886,12.222976684570312;43.153877985857655,12.199974060058596;43.17916927309263,12.178688049316408;43.183675531809236,12.15190887451172;43.19043429625546,12.07878112792969;43.20419983484575,12.082214355468752;43.211707001539125,12.08770751953125;43.21120655250451,12.138862609863283;43.195940884339194,12.188987731933594;43.193688249268355,12.224693298339846;43.20169724062843,12.243919372558596;43.21120655250451,12.335243225097658;43.173160410762925,12.367858886718752;43.14511123534926,12.359275817871096&mode=rss';

        // loop through N pages
        for ($i = 0; $i < 2; $i++) {

            $url = str_replace(
                '{{page}}',
                $i+1,
                $urlFormatStr
            );

            try {
                $this->refresh($url, $i+1);
            } catch (Exception $e) {
                printf("\n=== Error ===\n");
                printf("page:%u file:%s line:%u\n", $i+1, $e->getFile(), $e->getLine());
                printf("error:%s\n\n", $e->getMessage());
            }

        }

        return 0;
    }

    private function refresh(string $dataURL, int $page): void
    {
        // if (!($xml = file_get_contents(base_path('rss.xml'))) {
        if (!($xml = file_get_contents($dataURL))) {
            throw new Exception(sprintf('Failed loading URL %s', $dataURL));
        }

        if (!($data = simplexml_load_string($xml))) {
            throw new Exception(sprintf('Failed decoding XML from URL %s', $dataURL));
        }

        foreach ($data->channel->item as $house) {

/*
23 => SimpleXMLElement {#721
+"title": SimpleXMLElement {#819}
+"link": SimpleXMLElement {#820}
+"pubDate": "2020-07-30 18:22"
+"description": SimpleXMLElement {#821}
*/

            $title = (string)$house->title;
            $link = (string)$house->link;
            $description = (string)$house->description;

            try{
                list($reference) = $this->parseLink($link);
            } catch (Exception $e) {
                printf("\n=== Error ===\n");
                printf("page:%u file:%s line:%u\n", $page, $e->getFile(), $e->getLine());
                printf("error:%s\n\n", $e->getMessage());
                continue;
            }

            $watch = Watch::where('reference', $reference)->first();

            if (!$watch) { // this property is not watched
                continue;
            }

            try {
                list($type, $price, $size) = $this->parseTitle($title);
            } catch (Exception $e) {
                printf("\n=== Error ===\n");
                printf("page:%u file:%s line:%u\n", $page, $e->getFile(), $e->getLine());
                printf("error:%s\n\n", $e->getMessage());
                continue;
            }

            try {
                list($location, $rooms, $img) = $this->parseDescription($description);
            } catch (Exception $e) {
                printf("\n=== Error ===\n");
                printf("page:%u file:%s line:%u\n", $page, $e->getFile(), $e->getLine());
                printf("error:%s\n\n", $e->getMessage());
                continue;
            }

            $house = $watch->house()->first();

            if (!$house) { // No records exist, create one...

                $house = House::create([
                    'watch_id' => $watch->id,
                    'property_type' => $type,
                    'size' => $size,
                    'rooms' => $rooms,
                    'location' => $location,
                    'img' => $img,
                    'description' => $description,
                ]);

                $house->prices()->save(new Price([
                    'amount' => $price
                ]));

                continue; // move on to the next
            }

            // check if the price changed

            $oldPrice = $house->prices()->orderBy(
                'id',
                'desc'
            )->first('amount')->amount;

            if ($oldPrice != $price) {

                if ($price == $oldPrice) {
                    continue;
                } elseif ($price < $oldPrice) {
                    $subject = 'House Price Decreased :)';
                } elseif ($price > $oldPrice) {
                    $subject = 'House Price Increased :(';
                }

                // format message for desktop notification
                $message = sprintf(
                    "Ref: %s
Location: %s
Type: %s (%u Rooms)
Old Price: €%s
New Price: €%s",
                    $watch->reference,
                    $house->location,
                    $house->property_type,
                    $house->rooms,
                    number_format($oldPrice, 0),
                    number_format($price, 0),
                    $link,
                    $link
                );

                // send desktop notification
                $this->notifyDesktop($subject, $message);

                // create new price record
                $house->prices()->save(new Price([
                    'amount' => $price
                ]));
            }

        }
    }

    private function notifyDesktop(string $title, string $content): void
    {
        $commandStr = <<< EOF
notify-send \
    -t 59000 \
    -u normal \
    "%s" \
    "%s" &
EOF;

        exec(sprintf($commandStr, $title, $content));

    }

    private function parseTitle(string &$title): array
    {
        //<title><![CDATA[Villa - € 145.000   250 mq]]></title>

        $regex = '/^([a-z\s\/]+)\s-\s€\s([0-9\.]+)\s+([0-9]+)\smq$/i';

        if (!preg_match($regex, $title, $match)) {
            throw new Exception(
                sprintf(
                    'Failed parsing title: %s',
                    $title
                )
            );
        }

        return [
            $match[1], // type : string
            str_replace('.', '', $match[2]), // price : int
            $match[3], // size : int
        ];
    }

    private function parseDescription(string &$description): array
    {
        //<description><![CDATA[<b>Corciano - Mantignana</b><br /><b>€ 197.000    330 mq    >5 locali</b><br /><img src="https://pic.im-cdn.it/image/906518927/xxs-c.jpg" border="0" /><br />CASA SEMINDIPENDENTE A MANTIGNANA Immersa nella natura poco al di sopra della zona residenziale di Mantignana, in zona altamente panoramica ma al tempo stesso a pochi minuti dalla zona ampiamente servita, disponiamo di questa interessante semi-indipendente, libera su tre lati tutti con giardino e confinante da un solo lato. La Villetta essendo pa... <a href='https://www.immobiliare.it/annunci/79630833/' target='_blank'>(leggi tutto)</a>]]></description>

        $regex = '/^<b>(.*)<\/b>.*([0-9]+)\slocali.*<img src="(.+)"\sborder="0"\s\/>/Ui';

        if (!preg_match($regex, $description, $match)) {
            throw new Exception(
                sprintf(
                    'Failed parsing description: %s...',
                    substr($description, 0, 30)
                )
            );
        }

        return [
            $match[1], // type : string
            $match[2], // num rooms : int
            $match[3], // img url : string
        ];
    }

    private function parseLink(string &$url): array
    {
        // https://www.immobiliare.it/annunci/81920196/

        $regex = '/https:\/\/www.immobiliare.it\/annunci\/([0-9]+)\//';

        if (!preg_match($regex, $url, $match)) {
            throw new Exception(
                sprintf(
                    'Failed parsing link: %s',
                    $url
                )
            );
        }

        return [
            $match[1]  // immobiliare.it ID : string
        ];
    }
}
