<?php

namespace App\Console\Commands;

use App\Watch;
use Illuminate\Console\Command;

class AddWatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hpm:add {reference}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a reference to watch (eg. 81920196)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reference = $this->argument('reference');

        $watch = Watch::where('reference', $reference)->first();

        if ($watch) {
            $this->error('Reference is already watched');
            return 1;
        }

        Watch::create([
            'reference' => $reference
        ]);

        $this->info('Reference added!');

        return 0;
    }
}
