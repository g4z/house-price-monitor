<?php

namespace App\Console\Commands;

use App\Watch;
use Illuminate\Console\Command;

class ListWatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hpm:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List currently watched references';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Watch::all() as $watch) {
            $this->info($watch->reference);
        }

        return 0;
    }
}
