<?php

namespace App;

use App\House;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'house_id',
        'amount',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be type cast
     *
     * @var array
     */
    protected $casts = [
        'house_id' => 'integer',
        'amount' => 'integer',
    ];

    /**
     * Return the user program
     *
     * @return BelongsTo
     */
    public function house()
    {
        return $this->belongsTo(House::class);
    }
}
