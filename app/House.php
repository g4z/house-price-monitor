<?php

namespace App;

use App\Price;
use App\Watch;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'watch_id',
        'property_type',
        'size',
        'rooms',
        'location',
        'img',
        'description',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be type cast
     *
     * @var array
     */
    protected $casts = [
        'watch_id' => 'integer',
        'property_type' => 'string',
        'size' => 'integer',
        'rooms' => 'integer',
        'location' => 'string',
        'img' => 'string',
        'description' => 'string',
    ];

    /**
     * Return the watch
     *
     * @return BelongsTo
     */
    public function watch()
    {
        return $this->belongsTo(Watch::class);
    }

    /**
     * Return the prices
     *
     * @return HasMany
     */
    public function prices()
    {
        return $this->hasMany(Price::class);
    }
}




