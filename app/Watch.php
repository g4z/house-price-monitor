<?php

namespace App;

use App\House;
use Illuminate\Database\Eloquent\Model;

class Watch extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be type cast
     *
     * @var array
     */
    protected $casts = [
        'reference' => 'string',
    ];

    /**
     * Return the house
     *
     * @return HasOne
     */
    public function house()
    {
        return $this->hasOne(House::class);
    }
}
